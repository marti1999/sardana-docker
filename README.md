# Sardana docker images


The primary purpose of these [Docker](http://www.docker.com) images is to use it for [Sardana](http://www.sardna-controls.org) testing in our [Continuous Integration workflow](https://travis-ci.org/sardana-org/sardana).

But you may also run it on your own machine to simply try sardana or even use it as an execution environment if you plan to develop sardana project.


There exist two flavors of the images:
- sardana only images 
- all-in-one image (deprecated)

## Sardana only images

These docker images contain the latest version of sardana
and its dependencies. Currently we provide images based on conda
and the remaining part of this chapter refers to them.
In the future we plan to provide similar images based on Debian
Linux distribution.

The conda directory includes a `Dockerfile` to build images using conda for different versions of Python. Those images are based on [micromamba](https://github.com/mamba-org/micromamba-docker) to keep the images not too big.

They include the stable version of `sardana` from pypi and its
dependencies from  [conda-forge](https://conda-forge.org/).
No `mysql` nor Tango `Databaseds`.

You can use them locally using the `docker-compose.yml` file included in this repository.

If you want to use a development version of sardana instead of 
the one installed in the container, you could uncomment the
`volumes` option in the `docker-compose.yml` file to mount the
sardana source repo and overwrite sardana with
`python -m pip install -e /build`. This will require that you
grant write permissions to all users to your local source repo:
`chmod -R a+w <source_repo_dir>`.

1. Start the 3 images by running `docker-compose up -d`.
2. Enter the `sardana` container and start the `Pool` and `MacroServer` inside that container. When started, press `CTRL-Z` and enter `bg` to put them in the background. Or enter the container in another terminal if you want to leave them in the foreground.

   ~~~bash
   docker exec -it sardana bash
   (base) mambauser@fddc6d2cc87d:/build$ Pool demo1
   demo1 does not exist. Do you wish to create a new one (Y/n) ?
   ^Z
   [1]+  Stopped                 Pool demo1
   (base) mambauser@fddc6d2cc87d:/build$ bg
   [1]+ Pool demo1 &
   (base) mambauser@fddc6d2cc87d:/build$ MacroServer demo1
   demo1 does not exist. Do you wish to create a new one (Y/n) ?

   Available Pools:
   Pool_demo1_1 (a.k.a. Pool/demo1/1) (running)

   Please select the Pool to connect to (return to finish): Pool_demo1_1
   Please select the Pool to connect to (return to finish):

   MacroServer demo1 has been connected to Pool/s ['Pool_demo1_1']

   ^Z
   [2]+  Stopped                 MacroServer demo1
   (base) mambauser@fddc6d2cc87d:/build$ bg
   [2]+ MacroServer demo1 &
   ~~~~

3. You can now run `spock` and `sar_demo`.

   ~~~~bash
   (base) mambauser@fddc6d2cc87d:/build$ spock --profile=demo1
   Profile 'demo1' does not exist. Do you want to create one now ([y]/n)? y
   Available Door devices from sardana_tango-db_1.sardana_default:10000 :
   Door_demo1_1 (a.k.a. Door/demo1/1) (running)
   Door name from the list? Door_demo1_1
   Storing ipython_config.py in /home/mambauser/.ipython/profile_demo1... [DONE]
   [TerminalIPythonApp] WARNING | Config option `deep_reload` not recognized by `TerminalInteractiveShell`.
   Spock 3.3.7-alpha -- An interactive laboratory application.

   help      -> Spock's help system.
   object?   -> Details about 'object'. ?object also works, ?? prints more.

   IPython profile: demo1

   Connected to Door_demo1_1

   Door_demo1_1 [1]: sar_demo
   Creating motor controller motctrl01 ...
   Created motctrl01
   ...
   ~~~~

4. When you are done, stop all the containers by running `docker-compose down`.

## All-in-one image

**This image is deprecated in favor of using sardana only
images with docker-compose.**

It is based on a [Debian](http://www.debian.org) stable and it provides the following infrastructure for installing and testing Sardana:

* sardana dependencies and recommended packages (PyTango & itango, taurus, ipython, ...)
* a Tango DB configured and running
* sardana demo environment: Pool and MacroServer populated with the sar_demo macro and some basic MacroServer environment variables


Before continuing, answer yourself which would be your use case, cause the way you create the container differs and there is no way to change between them other than recreating the container from scratch.

## How to try sardana using this image

To run the container on your host simply execute:

~~~~
docker run -d --name=sardana-docker -h sardana-docker registry.gitlab.com/sardana-org/sardana-docker
~~~~

... or, if you want to launch GUI apps from the container and do **not mind about X security**:

~~~~
xhost +local:
docker run -d --name=sardana-docker -h sardana-docker -e DISPLAY=$DISPLAY -e QT_X11_NO_MITSHM=1 -v /tmp/.X11-unix:/tmp/.X11-unix registry.gitlab.com/sardana-org/sardana-docker
~~~~

Then you can log into the container with:

~~~~
docker exec -it sardana-docker bash
~~~~

Note: this image does not contain sardana itself (since it is designed for installing development versions of sardana) but you can install it easilly using any of the following examples in your container (for more details, see http://www.sardana-controls.org/users/getting_started/index.html).:

- ~Example 1: installing sardana from the official debian repo~ (currently not available yet in Jessie backports):

~~~~
apt-get install python3-sardana -y
~~~~

- Example 2: installing the latest develop version from the git repo (you may use any other branch instead of develop):

~~~~
git clone -b develop https://gitlab.com/sardana-org/sardana.git
cd sardana
python3 setup.py install
~~~~

- Example 3: using pip to do the same as in example 2:

~~~~
pip3 install git+https://gitlab.com/sardana-org/sardana.git@develop
~~~~

## How to develop sardana using this image

This image may be also involved in the development process, for example to execute inside of the container the code under development on the host machine.
In order to make the code available within the container it must be mounted as a volume on the container instantiation (this example uses `/sardana` directory as the mounting point, but it may be any other directory):

~~~~
docker run -d --name=sardana-docker -h sardana-docker -v <path-to-sardana-code-on-host-machine>:/sardana registry.gitlab.com/sardana-org/sardana-docker
~~~~

Afterward the sardana should be installed in the develop mode:

~~~~
docker exec sardana-docker bash -c "cd /sardana && python3 setup.py develop"
~~~~

## Start the Sardana system

Regardless of the option that you had chosen for the Sardana installation (simply try Sardana or prepare a development environment) now is the time to start your Sardana system. The Sardana architecture used in this examples assumes Pool and MacroServer as separate servers, but remember that you can [run Sardana as a single server](https://sardana-controls.org/users/getting_started/running_server.html#running-sardana-as-a-tango-server) as well!

You can run the servers using supervisor (it will run them in background and exit):
~~~~
docker exec sardana-docker supervisorctl start Pool
docker exec sardana-docker supervisorctl start MacroServer
~~~~

Or if you are interested in keeping the processes in foreground, for example to see their stdout or stderr:
~~~~
docker exec sardana-docker Pool demo1
docker exec sardana-docker MacroServer demo1
~~~~

Right after that it is possible to run any sardana client applications within the container e.g. spock or macroexecutor - see running GUI applications section above:
~~~~
docker exec -it sardana-docker spock # start spock inside of this bash session
docker exec sardana-docker macroexecutor
~~~~
or any other...
